<?php

namespace App\Listeners;

use App\Events\OrganisationCreatedEvent;
use App\Notifications\OrganisationCreatedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

/**
 * Class SendOrganisationCreatedNotification
 * @package App\Listeners
 */
class SendOrganisationCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param OrganisationCreatedEvent $event
     * @return void
     */
    public function handle(OrganisationCreatedEvent $event)
    {
        $event->user->notify(new OrganisationCreatedNotification());
    }
}
