<?php

declare(strict_types=1);

namespace App\Transformers;

use App\Organisation;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * Class OrganisationTransformer
 * @package App\Transformers
 */
class OrganisationTransformer extends TransformerAbstract
{
    /**
     * @param Organisation $organisation
     *
     * @return array
     */
    public function transform(Organisation $organisation): array
    {
        return [
            'name' => $organisation->name,
            'owner' => $organisation->owner->name,
            'trial_end' => is_object(
                $organisation->trial_end) ?
                $organisation->trial_end->format('U') :
                ($organisation->trial_end !== null ?
                    strtotime((string)$organisation->trial_end) :
                    ''),
        ];
    }
}
