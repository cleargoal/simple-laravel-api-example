<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\AuthenticateUserRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\AuthService;

/**
 * Class AuthController
 * @package App\Controllers
 */
class AuthController extends ApiController
{
    protected $client;

    /**
     * Constructor of this class AuthController
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);

        $this->client = DB::table('oauth_clients')
            ->where(['password_client' => 1])
            ->first();
    }

    /**
     * Authenticate user in API
     *
     * @param AuthenticateUserRequest $request
     * @param AuthService $authService
     * @return JsonResponse
     */
    public function authenticate(AuthenticateUserRequest $request, AuthService $authService): JsonResponse
    {
        return response()->json( $authService->authApiUser($request->only('email', 'password')));
    }
}
