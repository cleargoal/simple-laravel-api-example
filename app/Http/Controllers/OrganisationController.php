<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrganisationRequest;
use App\Http\Requests\OrganisationFilterRequest;
use App\Services\OrganisationService;
use Illuminate\Http\JsonResponse;

/**
 * Class OrganisationController
 * @package App\Http\Controllers
 */
class OrganisationController extends ApiController
{
    /**
     * Create Organisation record method
     *
     * @param CreateOrganisationRequest $request
     * @param OrganisationService $service
     *
     * @return JsonResponse
     */
    public function create(CreateOrganisationRequest $request, OrganisationService $service): JsonResponse
    {
        return response()->json($service->formatOrganResponse($request->all()));
    }

    /**
     * List all records by filter without pagination
     *
     * @param OrganisationFilterRequest $filterRequest
     * @param OrganisationService $service
     * @return JsonResponse
     */
    public function listAll(OrganisationFilterRequest $filterRequest, OrganisationService $service): JsonResponse
    {
        $filter = $filterRequest->has('filter') ? $filterRequest->input('filter') : 'all';
        $organisations = $service->listAllOrganisations($filter);
        return response()->json($organisations);
    }
}
