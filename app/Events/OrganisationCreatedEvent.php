<?php

namespace App\Events;

use App\Organisation;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class OrganisationCreatedEvent
 * @package App\Events
 */
class OrganisationCreatedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The authenticated user.
     *
     * @var Authenticatable
     */
    public $user, $organisation;

    /**
     * Create a new event instance.
     *
     * @param Authenticatable $user
     * @param Organisation $organisation
     */
    public function __construct(Authenticatable $user, Organisation $organisation)
    {
        $this->user = $user;
        $this->organisation = $organisation;
    }
}
