<?php

declare(strict_types=1);

namespace App\Services;

use App\Events\OrganisationCreatedEvent;
use App\Organisation;
use App\Transformers\OrganisationTransformer;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * Class OrganisationService
 * @package App\Services
 */
class OrganisationService
{
    private $organisationTransformer;

    /**
     * Constructor of this service class
     *
     * @param OrganisationTransformer $organisationTransformer
     * @return void
     */
    public function __construct(OrganisationTransformer $organisationTransformer)
    {
        $this->organisationTransformer = $organisationTransformer;
    }

    /**
     * Format Organisation Response
     *
     * @param
     * @return array|string[]
     */
    public function formatOrganResponse($data): array
    {
        if (!isset($data) || empty($data)) {
            $response = [
                'result' => 'Missing data',
                'message' => 'Please, enter your organisation name',
                'dataset' => '',
            ];
        } elseif ($dataset = $this->createOrganisation($data)) {
            $response = [
                'result' => 'Success!',
                'message' => 'Organisation added successfully!',
                'dataset' => $dataset,
            ];
        } else {
            $response = [
                'result' => 'Error!',
                'message' => 'Try again, please.',
                'dataset' => '',
            ];
        }
        return $response;
    }

    /**
     * Create Organisation
     *
     * @param array $attributes
     * @return array
     */
    public function createOrganisation(array $attributes): array
    {
        $user = Auth::guard('api')->user();

        $organisation = new Organisation();
        $organisation->name = $attributes['name'];
        $organisation->owner_user_id = $user->id;
        $organisation->trial_end = Carbon::now()->addDays(30);
        $organisation->save();
        event(new OrganisationCreatedEvent($user, $organisation));
        return $this->organisationTransformer->transform($organisation);
    }

    /** List All Organisations by filter without pagination
     *
     * @param String $filter Filter of organizations list: 'subbed', 'trial', 'all'.
     * @return mixed
     */
    public function listAllOrganisations(string $filter)
    {
        $query = Organisation::with('owner');

        $subbed = ['subbed' => 1, 'trial' => 0];
        if (array_key_exists($filter, $subbed)) {
            $query = $query->where('subscribed', $subbed[$filter]);
        }
        $organs = $query->get();
        return $organs->map(function ($item) {
            return $this->organisationTransformer->transform($item);
        });
    }
}
