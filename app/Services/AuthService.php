<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

/**
 * Class OrganisationService
 * @package App\Services
 */
class AuthService
{
    /**
     * Authenticate user through API
     *
     * @param
     * @return array
     */
    public function authApiUser($credentials): array
    {
        if (!Auth::attempt($credentials)) {
            $response = [
                'message' => 'You cannot sign with those credentials',
                'errors' => 'Unauthorised'
            ];
        }
        else {
            $token = Auth::user()->createToken(config('app.name'));
            $token->token->expires_at = Carbon::now()->addHour();
            $token->token->save();
            $response = [
                'token_type' => 'Bearer',
                'token' => $token->accessToken,
                'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString()
            ];
        }        return $response;
    }
}
