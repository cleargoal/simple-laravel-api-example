<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Organisations</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .org-list {
            position: absolute;
            top: 1rem;
            left: 2rem;
            width: 35%;
            min-width:25%;
        }

        .filters {
            display: flex;
            flex-direction: row;
        }

        .real-table {
            display: flex;
            flex-wrap: wrap;
            margin: 0 0 3em 0;
            padding: 0;
        }

        .real-table--3cols > .real-table-cell {
            width: 33.33%;
        }

        .real-table-cell {
            box-sizing: border-box;
            flex-grow: 1;
            width: 100%;
            padding: 0.6em 0.4em;
            overflow: hidden;
            list-style: none;
            border: solid 3px white;
            background: #ccd;
        }

        .table-wrap {
            display: flex;
            flex-direction: column;
            max-height: 53rem;
            /*width: 40%;*/
            overflow-y: scroll;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="top-right links">
        @auth
            <a href="{{ url('/home') }}">Home</a>
        @else
            <form method="post">
                <input type="text" name="login" id="login-field" placeholder="Login">
                <input type="password" name="password" id="pass-field" placeholder="Password, min 8 symbols">
            </form>
            <a href="{{ route('api.login') }}" id="login-action">Log in</a>
        @endauth
    </div>

    <div class="org-list">
        <h2 style="width: 25rem;">View the organisations list: <span id="org-amount"></span></h2>
        <div class="table-wrap">
            <div class="filters">
                <form id="filter-form">
                    <span>Filters:</span>
                    <input type="radio" name="filter" value="all" class="radio" checked>All
                    <input type="radio" name="filter" value="subbed" class="radio">Subbed
                    <input type="radio" name="filter" value="trial" class="radio">Trial
                </form>
            </div>
            <div class="table-wrap">
                <div class="real-table real-table--3cols" id="organs">
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="title m-b-md">
            Organisations
        </div>
        <div>
            <form method="post">
                <input type="text" name="name" id="org-name">
                <input type="submit" id="org-submit">
            </form>
        </div>

    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
<script>
    const loginInput = document.querySelector('#login-field');
    const passInput = document.querySelector('#pass-field');
    const loginAct = document.querySelector('#login-action');
    const loginUrl = loginAct.getAttribute('href');
    const listAllUrl = "{{route('organs.list')}}";
    const createOrgUrl = "{{route('create.organ')}}";
    const orgInput = document.querySelector('#org-name');
    const orgSubmit = document.querySelector('#org-submit');
    const filterForm = document.querySelector('#filter-form');
    const orgAmount = document.querySelector('#org-amount');
    const orgList = document.querySelector('#organs');
    let orgAmountValue = 0;
    let filter = 'all';

    // login
    loginAct.addEventListener('click', function (e) {
        e.preventDefault();
        let loginValue = loginInput.value;
        let passValue = passInput.value;
        axios.post(loginUrl, {
            email: loginValue,
            password: passValue
        })
            .then(response => {
                let responseData = response.data;
                if (responseData.token_type === 'Bearer') {
                    sessionStorage.setItem('bearerToken', responseData.token);
                    alert('Authentication successful!');
                    getOrgans();
                }
            })
            .catch(error => {
                let errMess = error.response.data.message;
                let errEmail = error.response.data.errors.email;
                let errPass = error.response.data.errors.password;
                let errorAlert = errMess + (errEmail !== undefined ? ',\n\n ' + errEmail : '') + (errPass !== undefined ? ',\n\n ' + errPass : '');
                alert(errorAlert);
            });
    });

    // set list filter
    filterForm.addEventListener('click', function (e) {
        console.log(e.target.value);
        getOrgans(e.target.value);
    });

    // add organisation
    orgSubmit.addEventListener('click', function (e) {
        e.preventDefault();
        let orgName = orgInput.value;

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem('bearerToken');
        axios.post(createOrgUrl, {name:orgName})
        .then(response=> {
            orgInput.value = '';
            addOrganRow(response.data.dataset);
            alert(response.data.result + '\n\n' + response.data.message);
        })
        .catch(error=> {
            if (error.response.status === 403) {
                alert('Authenticate you please');
            } else if(error.response.status === 422) {
                alert(error.response.data.errors.name);
            }
            else {
                alert(error.message);
            }
        });
    });

    // do request to backend
    function getOrgans(filter) {
        orgAmountValue = 0;
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem('bearerToken');
        axios.get(listAllUrl, {params:{filter: filter}})
            .then(response => {
                drawOrgansList(response.data);
            })
            .catch(error => {
                if (error.response.status === 403) {
                    alert('Authenticate you please');
                } else {
                    alert(error.message);
                }
            })
    }

    // go through organisations collection
    function drawOrgansList(data) {
        const heads = ['Organisation', 'Owner', 'Trial end Date'];
        orgList.innerHTML = '';
        heads.forEach((element) => {
            let headerCell = document.createElement('div');
            headerCell.classList.add('real-table-cell');
            headerCell.innerHTML = '<h3>' + element + '</h3>';
            orgList.append(headerCell);
        });

        data.forEach((row) => {
            addOrganRow(row);
        });
    }

    // add 1 row to organisations list
    function addOrganRow(row) {
        orgAmountValue++;
        orgAmount.innerText = orgAmountValue;
        let rowValues = Object.values(row);
        rowValues.forEach((cellValue) => {
            let valueCell = document.createElement('div');
            valueCell.classList.add('real-table-cell');
            valueCell.innerText = cellValue;
            orgList.append(valueCell);
        });
    }

    if (sessionStorage.getItem('bearerToken')) {
        getOrgans();
    }
    else {
        alert('1st Enter: Authenticate you please');
    }
</script>
</body>
</html>
